class CfgPatches
{
	class dzr_unpack_dbo_bike_Scripts
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {"DZR_PackedBicycle"};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_unpack_dbo_bike
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_unpack_dbo_bike";
		name = "dzr_unpack_dbo_bike";
		dependencies[] = {"World","Game","Mission"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_unpack_dbo_bike/4_World"};
			};			
			class gameScriptModule
			{
				files[] = {"dzr_unpack_dbo_bike/3_Game"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_unpack_dbo_bike/5_Mission"};
			};
		};
	};
};

class CfgVehicles
{
	class Inventory_Base;
	class TireRepairKit;
	class DZR_PackedBicycle: TireRepairKit
	{
		scope = 2;
		displayName = "$STR_DZR_PackedBike";
		descriptionShort = "$STR_DZR_PackedBikeDesc";
		//quantityBar = 0;
		repairKitType = 0;
		weight = 7000;
	};
};

class CfgSoundShaders
{
	class TearUp_BaseSound
	{
		range=80;
		volume=2;
		distance=9;
	};
	class DZR_Unpack_Bicycle_Shader: TearUp_BaseSound
	{
		volume=1.8;
		samples[]=
		{
			
			{
				"\dzr_unpack_dbo_bike\sounds\BicycleUnpack",
				1
			}
		};
	};
};
class CfgSoundSets
{
	class Unpack_Bicycle_baseSound
	{
		distance=5;
		loop=0;
		spatial=1;
	};
	class Unpack_Bicycle_start: Unpack_Bicycle_baseSound
	{
		soundShaders[]=
		{
			"DZR_Unpack_Bicycle_Shader"
		};
		frequencyRandomizer=0;
		volumeRandomizer=0;
		volumeFactor=1;
	};
};
